public class Lab5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// 1.
		score(80);

		// Optional 1
		Floor("5");
		
		// Optional 2
		Size(29);
		
	}
	
	public static void score(int score) {
		switch(score) {
		case 80:
			System.out.println("A");
			break;
		case 70:
			System.out.println("B");
			break;
		case 60:
			System.out.println("C");
			break;
		case 50:
			System.out.println("D");
			break;
		case 40:
			System.out.println("F");
			break;	
		default:
			System.out.println("E");
		}
	}
	
	
	public static void Floor(String Floor) {
		switch(Floor) {
		case "5":
			System.out.println("5");
			break;
		case "4":
			System.out.println("4");
			break;
		case "3":
			System.out.println("3");
			break;
		case "2":
			System.out.println("2");
			break;
		case "G":
			System.out.println("G");
			break;	
		default:
			System.out.println("");
		}
	}
	
	public static void Size(int size) {
		switch(size) {
		case 29:
			System.out.println("Small");
			break;
		case 42:
			System.out.println("Medium");
			break;
		case 44:
			System.out.println("Large");
			break;
		case 48:
			System.out.println("Extra Large");
			break;
		default:
			System.out.println("Unknown");
		}
	}
}

