
public class Lab1 {
	public static void main(String[] args) {
		//ข้อ1
		
		// Hello World
		
		/*
			Hello World
		*/
		
		//ข้อ2
		int num = 1;
		boolean check = true ;
		String name = "Kamin";
		
		//ข้อ3
		System.out.println("This is a Integer: "+num);
		System.out.println("This is a Boolean: "+check);
		System.out.println("This is a String: "+name);
		
		//Lab1 Optinal
		double miles = 100;
		final double MILES_TO_KILOMETER = 1.609;
		double kilometer = miles * MILES_TO_KILOMETER;
		System.out.println("kilometer: "+kilometer);
	}
}