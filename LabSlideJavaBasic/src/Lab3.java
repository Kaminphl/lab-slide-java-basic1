
public class Lab3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//2
		int i = 20;
		System.out.println(i);
		i++;
		i++;
		i++;
		i++;
		i++;
		// i += 5;
		System.out.println(i);
		i--;
		i--;
		i--;
		i--;
		i--;
		// i -= 5;
		System.out.println(i);
		System.out.println();
		//2
		/*
		 Error
		System.out.println(2.00f && 3.00f);
		System.out.println(2 || 3);
		*/
		System.out.println(true && false);
		System.out.println(true || true);
		System.out.println();
		
		//optional 1
		OddEven(1);
		OddEven(2);
		OddEven(3);
		OddEven(4);
		OddEven(5);
		System.out.println();
		
		//optional 2
		CheckEqual(2,10);
		CheckEqual(12,12);
	}
	
	public static void OddEven(int num){
		if(num%2==1) {
			System.out.println("This is Odd Number");
		}else {
			System.out.println("This is Even Number");
		}
	}

	public static void CheckEqual(int first,int second){
		if(first == second) {
			System.out.println("เท่ากัน");
		}else {
			System.out.println("ไม่เท่ากัน");
		}
	}
	
}
