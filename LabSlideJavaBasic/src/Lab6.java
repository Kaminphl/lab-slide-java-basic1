
public class Lab6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// 1 & 2
		int i = 1;
		int Sum = 0;
		while(i<=10){
			Sum += i ;
			System.out.println("i = "+i);
			i++;
		}
		System.out.println("SUM: "+Sum);
		
		// 3
		
		i = 1;
		while(i<=100){
			if(i%12==0) {
				System.out.println("i = "+i);
			}
			i++;
		}
		
		//4 
		int[] num = {1,2,3,4,5};
		for(int Count: num) {
			System.out.println("Counter: "+Count);
		}
		
		// Optinal 1
		
		int Med = 0;
		i=1;
		while(i<=24){
			if(Med==12) {
				System.out.println("ทานยาครบแล้ว อย่าทานยาเกินขนาด");
			}
			
			if(i%2==0 && Med <=12 ) {
				Med++;
				System.out.println("ทานยาเม็ดที่: "+Med);
			}

			i++;
		}
		
		// Optinal 2
		
		String[] Month =  {"จันทร์","อังคาร","พุทธ","พฤหัสบดี","ศุกร์","เสาร์","อาทิตย์"} ;
		for(String S: Month) {
			System.out.println(S);
		}
		
	}

}
