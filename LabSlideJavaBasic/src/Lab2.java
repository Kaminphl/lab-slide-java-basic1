
public class Lab2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// ข้อ1
		bark();
		// ข้อ2
		float numFloat = 1.05f ;
		int numInteger = 1;
		double numDB = 100.50;
		char flag = 'Y' ;
		
		System.out.println( (int)numFloat );
		System.out.println( (float)numInteger );
		System.out.println( (float)numDB );
		System.out.println( (int)flag );
		// ข้อ3
		final String word = "Hello";
		
		// Error can't chage Value
		//word = "Hello World";
				
		// optional 1
		int myWeight = 74 ;
		float myFloatNum = 8.99f ;
		char myLetter = 'A' ;
		boolean myBool = false ;
		String myText = "Hello World" ;
			
		// optional 2
		double numDouble = 124.23;
		int numInt = (int)numDouble;
		System.out.println(numInt);
	
	}
	
	public static void bark() {
		String dogName = "Momo";
		System.out.println("The Dog name = "+dogName+"bark");
	}
	
}